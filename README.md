# Micropython

Some libraries for micropython. Tested mostly on ESP8266 and ESP32.

## Suggestions

- The memory of ESP8266 is very limited. Some of the libraries are rather big and precompiling with mpy_cross can save a lot of memory.

# License
For a copy of the License see the file LICENSE. This is the valid License if nothing else is stated in the code.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.
