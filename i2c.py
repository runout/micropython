# Copyright (c) 2022 Gschwendt IT-Consulting
# Author: Markus Gschwendt
# License: GPLv3
#

'''I2C class which extends the standard I2C class from micropython
and can handle I2C multiplexers like TCA9548A
by adding 12bit to the I2C address.
It works with machine.I2C and machine.SoftI2C too.

Backward compatible for usage without I2C extender chip.

Warning: The scan() method does not work as expected and is not 100% compatible to the original library.
Without an address/channel parameter it does not know which muliplexer and channel to select.
'''

from ustruct import pack

try:
    from machine import I2C as MachineI2C
    if __debug__: print('load SoftI2C')
except:
    from machine import SoftI2C as MachineI2C
    if __debug__: print('load I2C')

class I2C(MachineI2C):
    '''changes to machine.I2C
    if addr > 0xff assume the address has following hex-format:
    8 bit multiplexer address, 4 bit channel, 8 bit target address
    
    eg 0x70140 for a htu21 sensor with address 0x40
    on channel 0 of a TCA9548A multiplexer with address 0x70
    
    channels are 1..8; corresponding to 0..7 on the TCA9548A
    channel 0 deselects all channels
    
    extract the address/channel by bit-shifting and and-operation.
    select the multiplexer channel
    deselect channel after read/write
    '''

    def selChannel(self, addr):
        
        if addr > 0xff:
            # extract address for multiplexer
            addr_m = (addr >> 12) & 0xff
            # extract channel
            ch = (addr >> 8) & 0xf
            chByte = b'\x00' if ch == 0 else pack('B', 1 << (ch - 1))
            try:
                super().writeto(addr_m, chByte)
                if __debug__: print(f'I2C: setting of channel {ch} on multiplexer {addr_m} for addr {addr & 0xff}')
            except:
                if __debug__: print(f'I2C: setting of channel {ch} on multiplexer {addr_m} failed', pack('B', 1 << ch))
                super().writeto(addr_m, b'\x00')
                return None
        if __debug__: print(f'I2C: {addr} -> {addr & 0xff}')
        return addr & 0xff


    def scan(self, addr=0x00):
        '''Scans the I2C bus for devices
        If an address including a TCA9548A address and a channel is given this channel will be scanned for devices.

        Warning: The scan() method does not work as expected and is not 100% compatible to the original library.
        Without an address/channel parameter it does not know which muliplexer and channel to select.
        In this case it scans only the bus without selecting a channel.

        To work around this problem in your code you can use something like this:

        if address & 0xff not in i2c.scan(address & 0xfff00):
            raise OSError(f"Device address {address} not found.")

        '''

        self.selChannel(addr)
        value = super().scan()
        self.selChannel(addr & 0xff0ff)
        return value


    def readfrom(self, addr, nbytes):
        value = super().readfrom(self.selChannel(addr), nbytes)
        self.selChannel(addr & 0xff0ff)
        return value
    
    
    def readfrom_into(self, addr, buf):
        value = super().readfrom_into(self.selChannel(addr), buf)
        self.selChannel(addr & 0xff0ff)
        return value


    def writeto(self, addr, buf):
        value = super().writeto(self.selChannel(addr), buf)
        self.selChannel(addr & 0xff0ff)
        return value


    def writevto(self, addr, vector, stop=True):
        value = super().writevto(self.selChannel(addr), vector)
        self.selChannel(addr & 0xff0ff)
        return value


    def readfrom_mem(self, addr, memaddr, nbytes, *, addrsize=8):
        value = super().readfrom_mem(self.selChannel(addr), memaddr, nbytes, addrsize=addrsize)
        self.selChannel(addr & 0xff0ff)
        return value
    

    def readfrom_mem_into(self, addr, memaddr, buf, *, addrsize=8):
        value = super().readfrom_mem_into(self.selChannel(addr), memaddr, buf, addrsize=addrsize)
        self.selChannel(addr & 0xff0ff)
        return value
    
    
    def writeto_mem(self, addr, memaddr, buf, *, addrsize=8):
        value = super().writeto_mem(self.selChannel(addr), memaddr, buf, addrsize=addrsize)
        self.selChannel(addr & 0xff0ff)
        return value