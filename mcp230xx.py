# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola, ported for Micropython ESP8266 by Cefn Hoile,
# better compatibility to machine.Pin and irq handling by Markus Gschwendt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

OUT     = 0
IN      = 1
HIGH    = True
LOW     = False

PUD_OFF = 0
PUD_DOWN= 1
PUD_UP  = 2

class MCP():
    """Base class to represent an MCP230xx series GPIO extender.  Is compatible
    with the Adafruit_GPIO BaseGPIO class so it can be used as a custom GPIO
    class for interacting with device.
    """

    def __init__(self, i2c, address=0x20, config=None):
        """Use MCP230xx on given i2c object at specified I2C address
        config  is a dictionay with config options
        {'intPin': Pin} pin object of machine which is connected to INTA or INTB
        """
        self.address = address
        self.i2c = i2c
            
        self.iocon = bytearray(1)  # chip config
        self.iocon[0] |= 1 << 6 # mirror INTA and INTB pins
        self.write_iocon()
        
        # Assume starting in ICON.BANK = 0 mode (sequential access).
        # Compute how many bytes are needed to store count of GPIO.
        self.gpio_bytes = self.NUM_GPIO//8
        # Buffer register values so they can be changed without reading.

        self.iodir = bytearray(self.gpio_bytes)  # Default direction to all inputs.
        self.gppu = bytearray(self.gpio_bytes)   # Default to pullups disabled.
        self.gpio = bytearray(self.gpio_bytes)
        self.gpinten = bytearray(self.gpio_bytes)  # enabling interrups
        self.intcon = bytearray(self.gpio_bytes) # Enable compare for interrupt triggers
        self.defval = bytearray(self.gpio_bytes) # Default to compare with for interrupt triggers
        self.intcap = bytearray(self.gpio_bytes)
        self.intf = bytearray(self.gpio_bytes)
        
        # Write current direction and pullup buffer state.
        self.write_iodir()
        self.write_gppu()

        # dict to hold pin objects: {pin_number: pin_object}
        self.pin_objects = {}
        # configure given machine pin for irq usage
        if config is not None:
            if 'intPin'in config:
                self.intPin = config['intPin'].irq(trigger=config['intPin'].IRQ_FALLING,
                                                   handler=self.int_callback)
            
    def _validate_pin(self, pin):
        """Promoted to mcp implementation from prior Adafruit GPIO superclass"""
        # Raise an exception if pin is outside the range of allowed values.
        if pin < 0 or pin >= self.NUM_GPIO:
            raise ValueError('Invalid GPIO value, must be between 0 and {0}.'.format(self.NUM_GPIO))

    def writeList(self, register, data):
        """Introduced to match the writeList implementation of the Adafruit I2C _device member"""
        return self.i2c.writeto_mem(self.address, register, data)

    def readList(self, register, length):
        """Introduced to match the readList implementation of the Adafruit I2C _device member"""
        return self.i2c.readfrom_mem(self.address, register, length)

    def setup(self, pin, value, pullup=True):
        """Set the input or output mode for a specified pin.  Mode should be
        either OUT or IN.
        For input pins: pullup=True activates the internal pull-up resistor.
        All other values will disable the pull-up resistor.
        """
        self._validate_pin(pin)
        # Set bit to 1 for input or 0 for output.
        if value == IN:
            self.iodir[int(pin/8)] |= 1 << (int(pin%8))
            pullup = True if pullup else False
            self.pullup(pin, pullup)
        elif value == OUT:
            self.iodir[int(pin/8)] &= ~(1 << (int(pin%8)))
        else:
            raise ValueError('Unexpected value.  Must be IN or OUT.')
        self.write_iodir()

    def output(self, pin, value):
        """Set the specified pin the provided high/low value.  Value should be
        either HIGH/LOW or a boolean (True = HIGH).
        """
        self.output_pins({pin: value})

    def output_pins(self, pins):
        """Set multiple pins high or low at once.  Pins should be a dict of pin
        name to pin value (HIGH/True for 1, LOW/False for 0).  All provided pins
        will be set to the given values.
        """
        [self._validate_pin(pin) for pin in pins.keys()]
        # Set each changed pin's bit.
        for pin, value in iter(pins.items()):
            if value:
                self.gpio[int(pin/8)] |= 1 << (int(pin%8))
            else:
                self.gpio[int(pin/8)] &= ~(1 << (int(pin%8)))
        # Write GPIO state.
        self.write_gpio()


    def input(self, pin, read=True):
        """Read the specified pin and return HIGH/True if the pin is pulled
        high, or LOW/False if pulled low.
        """
        return self.input_pins([pin], read)[0]

        
    def input_pins(self, pins, read=True):
        """Read multiple pins specified in the given list and return list of pin values
        HIGH/True if the pin is pulled high, or LOW/False if pulled low.
        """
        [self._validate_pin(pin) for pin in pins]
        if read:
            # Get GPIO state.
            self.read_gpio()
        # Return True if pin's bit is set.
        return [(self.gpio[int(pin/8)] & 1 << (int(pin%8))) > 0 for pin in pins]


    def intpin(self, pinObj, trigger=None):
        pin = pinObj.pin
        self._validate_pin(pin)
        if trigger in (pinObj.IRQ_RISING, pinObj.IRQ_FALLING, pinObj.IRQ_RISING | pinObj.IRQ_FALLING):
            '''enable interrupt'''
            self.gpinten[int(pin/8)] |= 1 << (int(pin%8))
            self.intcon[int(pin/8)] &= ~(1 << (int(pin%8)))
            self.pin_objects[pin] = pinObj
        else:
            '''disable interrupt'''
            self.gpinten[int(pin/8)] &= ~(1 << (int(pin%8)))
            if pin in self.pin_objects:
                del self.pin_objects[pin]
        self.write_intpin()
        
        
    def pullup(self, pin, enabled):
        """Turn on the pull-up resistor for the specified pin if enabled is True,
        otherwise turn off the pull-up resistor.
        """
        self._validate_pin(pin)
        if enabled:
            self.gppu[int(pin/8)] |= 1 << (int(pin%8))
        else:
            self.gppu[int(pin/8)] &= ~(1 << (int(pin%8)))
        self.write_gppu()

    def read_gpio(self):
        self.gpio = bytearray(self.readList(self.GPIO, self.gpio_bytes))

    def read_intcap(self):
        self.intcap = bytearray(self.readList(self.INTCAP, self.gpio_bytes))

    def read_intf(self):
        self.intf = bytearray(self.readList(self.INTF, self.gpio_bytes))

    def write_gpio(self, gpio=None):
        """Write the specified byte value to the GPIO register.  If no value
        specified the current buffered value will be written.
        """
        if gpio is not None:
            self.gpio = bytearray(gpio)
        self.writeList(self.GPIO, self.gpio)

    def write_iodir(self, iodir=None):
        """Write the specified byte value to the IODIR register.  If no value
        specified the current buffered value will be written.
        """
        if iodir is not None:
            self.iodir = bytearray(iodir)
        self.writeList(self.IODIR, self.iodir)

    def write_gppu(self, gppu=None):
        """Write the specified byte value to the GPPU register.  If no value
        specified the current buffered value will be written.
        """
        if gppu is not None:
            self.gppu = bytearray(gppu)
        self.writeList(self.GPPU, self.gppu)

    def write_intpin(self):
        """Write to interrupt registers
        """
        self.writeList(self.GPINTEN, self.gpinten)
        self.writeList(self.INTCON, self.intcon)
        self.writeList(self.DEFVAL, self.defval)

    def write_iocon(self, iocon=None):
        """Write the specified byte value to the DEFVAL register
        """
        if iocon is not None:
            self.iocon = bytearray(iocon)
        self.writeList(self.IOCON, self.iocon)

    def int_callback(self, p):
        """callback for interrupt handler
        """
        self.read_intf()
        if int.from_bytes(self.intf, 'big') == 0:
            return
        self.read_gpio()
        #self.read_intcap()
        for pin in  range(self.NUM_GPIO):
            if (self.intf[int(pin/8)] & 1 << int(pin%8)
                and pin in self.pin_objects
                and type(self.pin_objects[pin]) is Pin
                and callable(self.pin_objects[pin].irq_callback)
            ):
                if ((self.pin_objects[pin].trigger == self.pin_objects[pin].IRQ_RISING | self.pin_objects[pin].IRQ_FALLING)
                    or (self.gpio[int(pin/8)] & 1 << int(pin%8) and self.pin_objects[pin].trigger == self.pin_objects[pin].IRQ_RISING)
                    or (~(self.gpio[int(pin/8)]) & 1 << int(pin%8) and self.pin_objects[pin].trigger == self.pin_objects[pin].IRQ_FALLING)
                
                ):
                    self.pin_objects[pin].irq_callback(self.pin_objects[pin])

class MCP23017(MCP):
    """MCP23017-based GPIO class with 16 GPIO pins."""
    # Define number of pins and register addresses.
    NUM_GPIO = 16
    IOCON    = 0x0A
    IPOL     = 0x02
    IODIR    = 0x00
    GPIO     = 0x12
    GPPU     = 0x0C
    INTCAP   = 0x10
    INTF     = 0x0E
    GPINTEN  = 0x04
    INTCON   = 0x08
    DEFVAL   = 0x06

class MCP23008(MCP):
    """MCP23008-based GPIO class with 8 GPIO pins."""
    # Define number of pins and register addresses.
    NUM_GPIO = 8
    IOCON    = 0x05
    IPOL     = 0x01
    IODIR    = 0x00
    GPIO     = 0x09
    GPPU     = 0x06
    INTCAP   = 0x08
    INTF     = 0x07
    GPINTEN  = 0x02
    INTCON   = 0x04
    DEFVAL   = 0x03
    

class Pin():
    '''Represents a pin for MCP class'''
    IN = 1
    OUT = 0
    IRQ_RISING = 1
    IRQ_FALLING = 2
    IRQ_BOTH = 3
        
    def __init__(self, mcp, pin, mode, pullup=True):
        '''mcp is a mcp opject
        gpio on the given mcp
        mode IN or OUT
        pullup activates/deactivates intternal pullup for the pin
        '''
        self.mcp = mcp
        self.mcp.setup(pin, mode, pullup)
        self.pin = pin
        self.irq_callback = None
        self.trigger = None

    def __repr__(self):
        return self.__class__.__name__ + 'MCP(' + str(self.mcp.address) + ',' + str(self.pin) + ')'
    
    def on(self):
        '''set output pin HIGH'''
        self.mcp.output(self.pin, HIGH)
        
    def off(self):
        '''set output pin LOW'''
        self.mcp.output(self.pin, LOW)

    def value(self, value=None):
        '''get or set value of pin'''
        if value == None:
            return self.mcp.input(self.pin, read=True)
        elif value:
            self.on()
        else:
            self.off()
            
    def irq(self, trigger, handler):
        '''Configure an interrupt handler
        trigger can be Pin.IRQ_RISING, Pin.IRQ_FALLING, Pin.IRQ_RISING | Pin.IRQ_FALLING
        '''
        self.trigger = trigger
        self.irq_callback = handler
        self.mcp.intpin(self, trigger)
